# Imaging and non-imaging spectroscopy: Techniques and analysis

**Strategies to improve regression modeling based on the LUCAS topsoil spectral library**

This repository is part of the final examination of the course M-GFP3 Imaging and non-imaging spectroscopy: Techniques and analysis, at the University of Leipzig. The repository contains the final report and the code used to perform the analysis. All code that lead towards our analysis results is inside the jupyter notebook [Biskamp_Loer_Kruse_NIRS_submission.ipynb](./code/Biskamp_Loer_Kruse_NIRS_submission.ipynb). The final pdf for submission can be found under [Biskamp_Loer_Kruse_NIRS_submission.pdf](./results/Biskamp_Loer_Kruse_NIRS_submission.pdf).

Authors: Bjarne Biskamp, Frieder Loer, Jannes Kruse